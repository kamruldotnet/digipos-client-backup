odoo.define('pos_edit_order_date.buttons', function (require) {
"use strict";
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var gui = require('point_of_sale.gui');
    var _t = core._t;

    var button_edit_order_date = screens.ActionButtonWidget.extend({
        template: 'button_edit_order_date',
        button_click: function () {
        }
    });

    screens.define_action_button({
        'name': 'edit_order_date',
        'widget': button_edit_order_date
    });


 });