from odoo import models, fields, api, _


class PurchaseReportVendor(models.TransientModel):
    _name = 'pos.sales.report.wizard'

    start_date = fields.Datetime(string="Start Date", required=True)
    end_date = fields.Datetime(string="End Date", required=True)
    category_ids = fields.Many2many('pos.category', string='Category', required=True)

    def print_pos_sales_report_category_wise(self):
        pos_sale_order = self.env['pos.order.line'].search([])
        pos_sale_order_groupby_dict = {}
        for category in self.category_ids:
            filtered_pos_sale_order = list(filter(lambda x: x.product_id.pos_categ_id == category, pos_sale_order))
            print('filtered_pos_sale_order ===', filtered_pos_sale_order)
            # filtered_by_date = list(filter(lambda x: x.order_id.date_order >= self.start_date and x.order_id.date_order <= self.end_date,
            #                                filtered_pos_sale_order))
            pos_sale_order_groupby_dict[category.name] = filtered_pos_sale_order

        final_dist = {}
        for category in pos_sale_order_groupby_dict.keys():
            pos_sale_data = []
            for order in pos_sale_order_groupby_dict[category]:
                temp_data = []
                temp_data.append(order.name)
                temp_data.append(order.product_id.name)
                temp_data.append(order.order_id.date_order)
                temp_data.append(order.order_id.amount_total)
                print('order.order_id.amount_total:',order.order_id.amount_total)
                temp_data.append(order.order_id.amount_paid)

                #temp_data.append(order.amount_total)
                pos_sale_data.append(temp_data)
            final_dist[category] = pos_sale_data
        datas = {
            'ids': self,
            'model': 'pos.sales.report.wizard',
            'form': final_dist,
            'start_date': self.start_date,
            'end_date': self.end_date
        }

        print('datas:', datas)
        return self.env.ref('pos_sale_report_category_wise.report_pos_sale_order').report_action([], data=datas)
