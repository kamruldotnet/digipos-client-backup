{
    'name': 'POS Sales Report Category Wise',
    'version': '12.0.0.0.1',
    'summary': 'POS Sale Report Category Wise.',
    "description": """
        POS sale report category wise
    """,
    'author': 'Kamrul Hasan',
    'maintainer': 'Kamrul Hasan',
    'Company': 'DigiPOS',
    'website': 'http://kamrul.net',
    'depends': ['base', 'contacts', 'point_of_sale'],
    'license': 'LGPL-3',
    'category': 'Purchase',
    'data': [
        'wizards/pos_sale_report_wizard.xml',
        'reports/pos_sale_report.xml',
        'reports/reports.xml',
    ],
    'images': [],
    'price': 0.0,
    'currency': 'EUR',
    'installable': True,
    'auto_install': False,
    'application': True,
    'sequence': 7,
}
