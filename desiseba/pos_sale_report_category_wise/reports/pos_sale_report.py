from odoo import fields, models, api

class PosSaleReport(models.AbstractModel):
    _name = 'report.pos_sale_report_category_wise.sale_report'

    @api.model
    def _get_report_values(self, docids, data=None):
        return {
            'doc_ids': data.get('ids'),
            'doc_model': data.get('model'),
            'data': data['form'],
            'start_date': data['start_date'],
            'end_date': data['end_date'],
        }