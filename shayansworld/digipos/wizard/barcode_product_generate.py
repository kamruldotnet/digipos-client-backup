from odoo import api, fields, models, _
from odoo.exceptions import UserError
import re
import math

class AutoGenerateBarcodeWizard(models.TransientModel):
    _name = "barcode.product.generate.wiz"
    _description = 'Barcode Product Generate Wizard'

    @api.model
    def default_get(self, fields):
        res = super(AutoGenerateBarcodeWizard, self).default_get(fields)
        active_ids = self._context.get('active_ids')
        sale_order_ids = self.env['product.product'].browse(active_ids)
        print("default_get:",sale_order_ids)
        '''barcode_order_lines = []
        for order in sale_order_ids:
            for line in order.order_line:
                barcode_order_lines.append((0, 0, {
                    'label_id': self.id,
                    'product_id': line.product_id.id,
                    'qty': line.product_uom_qty or 1,
                }))
        res.update({
            'product_barcode_ids': barcode_order_lines
        })'''
        return res

    @api.multi
    def generate_product_barcode(self):
        print('Confirm barcode generator')
        self.ensure_one()
        product_ids = self.env['product.product'].browse(self._context.get('active_ids'))

        for prod in product_ids:
            product_id = prod.id
            print(product_id)
            eanbarcode = generate_ean(str(product_id))
            self.env.cr.execute("update product_product set barcode='"+ str(eanbarcode) +"' where id='"+ str(product_id) +"'")

@api.multi
def ean_checksum(eancode):
    """returns the checksum of an ean string of length 13, returns -1 if
    the string has the wrong length"""
    if len(eancode) != 13:
        return -1
    oddsum = 0
    evensum = 0
    eanvalue = eancode
    reversevalue = eanvalue[::-1]
    finalean = reversevalue[1:]

    for i in range(len(finalean)):
        if i % 2 == 0:
            oddsum += int(finalean[i])
        else:
            evensum += int(finalean[i])
    total = (oddsum * 3) + evensum

    check = int(10 - math.ceil(total % 10.0)) % 10
    return check

def generate_ean(ean):
    """Creates and returns a valid ean13 from an invalid one"""
    if not ean:
        return "0000000000000"
    ean = re.sub("[A-Za-z]", "0", ean)
    ean = re.sub("[^0-9]", "", ean)
    ean = ean[:13]
    if len(ean) < 13:
        ean = ean + '0' * (13 - len(ean))
    return ean[:-1] + str(ean_checksum(ean))