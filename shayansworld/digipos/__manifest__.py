{
    'name':'DigiPOS - The point of sale solution',
    'version':'12.0',
    'category':'Point of Sale',
    'summary': 'Point of sale solution',
    'sequence': '0',
    'author': 'Kamrul Hasan',
    'company': 'Digitalsunrun',
    'maintainer': 'Digitalsunrun',
    'support': 'khasancsit@gmail.com',
    'website': 'http://digiposbd.com',
    'depends': [
        'point_of_sale','stock','purchase','website',
        ],
    'live_test_url': '',
    'demo': [],
    'data': [
        #'views/product_label.xml',
        'views/website_login.xml',
        #'views/inventory_product.xml',
        'wizard/barcode_product_generate_view.xml',

    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [],
    'qweb': [
        'static/src/xml/pos_receipt_view.xml',
        'static/src/xml/digipos_pos_asserts.xml'
    ],
}