
=> 12.0.0.1 : Improved index video link.

Version 12.0.0.2 : (02/06/20)
		- Print only related attributes of product variant in "Barcode Product Labels" for print in product variants,sale,purchase and stock.
		- Remove _description warnings.